# T_NSI_21

Dépôt de fichiers de __terminale NSI__ Lycée M de Flandre à Gondecourt  
C. Ghesquiere : cecile.ghesquiere@ac-lille.fr 




<table class="tftable" border="1">
<tr><th>chapitre</th><th>programme</th><th>déroulé</th></tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-007.gif"></td>
<td style="color:#8973c1" >Algorithmique</span></td>
<td>
 <b>Recherche textuelle </b> </br>
<ul><li> Cours  <a href ="./Chapitres/C16_recherche_textuelle/C16_cours/C16_cours.md"> cours </a>  </li>
<li> exercices :  <a href ="./Chapitres/C16_recherche_textuelle/exos/C16_exos.md"> md </a>   </li> 
</td>
</tr>
<tr>
<td>Révision</td>
<td ><span> sujet polynesie 2022 </span></td>
<td> 
<a href="./ex_bac/JIPO1.pdf">jour 1 </a> et une proposition de  <a href="https://ens-fr.gitlab.io/algo2/9-BAC/1-jipo1/">  corrigé  </a>  depuis le site de l'ens <a href="./ex_bac/JIPO2.pdf">jour 2 </a>
</td>
</tr>

<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-006.gif"></td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td>
 <b>Sécurité des communications </b> </br>
<ul><li> Cours  <a href ="./Chapitres/C15_securite_comm/C15_securisation_communication.ppsx"> diaporama </a>  </li>
<li> TP Signature et chiffrage d'un fichier :  <a href ="./Chapitres/C15_securite_comm/C15_TP_SHA.md"> md </a>   </li> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Structure de données </span></td>
<td> 
<a href="./ex_bac/Sujet2 _Met_sept_2021_ex 4.pdf">Métropole 1 Septembre 2021 ex 4  POO </a> 
</td>
</tr>
<tr>
<td>Révision</td>
<td ><span> Algorithmique Langage et programmation </span></td>
<td> 
<a href="./ex_bac/Sujet1_2021_Met_Sep_Ex2.pdf">Métropole 1 Septembre 2021 ex 2  Recherche dichotomique </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Structures de données </span></td>
<td> 
<a href="./ex_bac/Met_sept_2_2021_ex2.pdf">Métropole 2 Septembre 2021 ex 2 Dictionnaires  ; spécification  </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Structures de données </span></td>
<td> 
<a href="./ex_bac/Met_sept_2_2021_ex4.pdf">Métropole 2 Septembre 2021 ex 4 Programmation Orientée Objet   </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Architecture matérielle OS Réseaux </span></td>
<td> 
<a href="./ex_bac/Sujet_1.pdf">sujet 1 : exercice 2 bash / processus / crypto  </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Architecture matérielle OS Réseaux </span></td>
<td> 
<a href="./ex_bac/Sujet0_ex5.pdf">sujet 0 : exercice 5 réseaux protocole RIP OSPF  </a> 
</td>
</tr>


<tr>
<td>Révision</td>
<td ><span>  Base de données </span></td>
<td> 
<a href="./ex_bac/Sujet0_ex4.pdf">sujet 0 : exercice 4 utilisant le SQL </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span>  Algorithme </span></td>
<td> 
<a href="./ex_bac/Sujet0_ex3.pdf">sujet 0 : exercice 3 sur arbres binaires de recherche </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Structure de données</span></td>
<td> 
<a href="./ex_bac/Sujet0_ex1.pdf">sujet 0 : exercice 1 sur Piles Python </a> 
</td>
</tr>
<tr>
<td>Révision</td>
<td ><span> Langage et programmation</span></td>
<td> 
<a href="./ex_bac/sujet_bac_2021_C_Etr_2_5.pdf">sujet 2 du bac Centre Etranger  </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Structures de données</span></td>
<td> 
<a href="./ex_bac/sujet_bac_2021_C_Etr_2_1.pdf">sujet 2 du bac Centre Etranger  </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Conversion decimal/binaire ; table de vérité ; codage des caractères</span></td>
<td> 
<a href="./ex_bac/Sujet_bac_2021_C_Etr_2_3.pdf">sujet 2 du bac Centre Etranger  </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Base de données </span></td>
<td> 
<a href="./ex_bac/sujet_bac_2021_C_Etr_2_4.pdf">sujet 2 du bac Centre Etranger  </a> 
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-005.gif"></td>
<td style="color:#e06666" > algorithmique </span></td>
<td> <b> Calculabilité</b> </br>
<ul>
<li> Cours  <a href ="./Chapitres/C14_calc/_PROF__Cours_Notions_sur_les_programmes-1.pdf"> pdf </a>   </li>
<li> vidéo à revoir <a href ="https://www.youtube.com/watch?v=92WHN-pAFCs"> lien </a>
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Algorithmique ; parcours tableaux</span></td>
<td> 
<a href="./ex_bac/Métrop_juin_21_algorithmique.pdf">sujet bac Métropole 2021 </a> 
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Pile POO</span></td>
<td> <ul>
<a href="./ex_bac/Métrop_juin_21_pile_POO.pdf">sujet bac Métropole 2021 </a> 
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-004.gif"></td>
<td style="color:#e06666" >Structure de données ; algorithmique </span></td>
<td> <b> Les graphes</b> </br>
<ul>
<li> Cours partie 1 <a href ="./Chapitres/C13_graphes/C13_1_cours.md"> md </a>  ou <a href ="./Chapitres/C13_graphes/C13_1_cours.pdf"> pdf </a>  </li>
<li> TP partie 1 :  <a href ="./Chapitres/C13_graphes/C13_1_TP_el.md"> md </a> ou <a href ="./Chapitres/C13_graphes/C13_1_TP_el.pdf"> pdf </a>  </li>
<li> Cours : partie 2  <a href ="./Chapitres/C13_graphes/C13_2_cours.md"> md </a>  ou <a href ="./Chapitres/C13_graphes/C13_2_cours.pdf"> pdf </a>  </li>
<li> Lien vers animations pour comprendre les parcours en profondeur et en largeur <a href ="http://fred.boissac.free.fr/AnimsJS/DariushGraphes/index.html"> lien (M Boissac) </a>
<li> Parcours de graphes  <a href ="./Chapitres/C13_graphes/Parcours_de_graphes.pdf"> pdf </a>  </li>
<li> Ours en cage : <a href ="./Chapitres/C13_graphes/ours_en_cage/ours_en_cage_el.md" > le sujet </a> et <a href ="./Chapitres/C13_graphes/ours_en_cage/ours_en_cage_el.zip" > le dossier à télécharger </li>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-003.gif"></td>
<td style="color:#17657D" >Langages et  programmation </span></td>
<td> <b>Mise au point et optimisation </b> </br>
<ul>
<li> Cours  <a href ="./Chapitres/C12_mise_au point/cours/C12_mise_au_point.md"> md </a>  ou <a href ="./Chapitres/cours/C12_mise_au point.pdf"> pdf </a>  </li>
<li> TP :  <a href ="./Chapitres/C12_mise_au point/tp/C12_TP.md"> md </a> ou <a href ="./Chapitres/C12_mise_au point/tp/C12_TP.pdf"> pdf </a>  </li> 
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"></td>
<td style="color:#bc6571" >Algorithmique</span></td>
<td> <b>Diviser pour régner </b> </br>
<ul>
<li> Cours  <a href ="./Chapitres/C11_diviser_regner/C11_diviser_regner.md"> md </a>  ou <a href ="./Chapitres/C11_diviser_regner/C11_diviser_regner.pdf"> pdf </a>  </li>
<li> TP :  <a href ="./Chapitres/C11_diviser_regner/C11_diviser_regner/TP/C11_TP_div_reg.md"> md </a> ou <a href ="./Chapitres/C11_diviser_regner/C11_diviser_regner/TP/C11_TP_div_reg.pdf"> pdf </a>  </li> 
<li> <a href ="./Chapitres/C11_diviser_regner/exo_div_reg_Met1_5.pdf"> sujet 1 Métropole exercice 4 </a> <a href ="./Chapitres/C11_diviser_regner/exo_sujet1_div_reg.pdf"> sujet 1  exercice 5 </a> </li>
<li> <a href = "./Chapitres/C11_diviser_regner/ex_qui_tire/qui_tire_sur_qui_el.md"> Qui tire sur qui ? </a> </li> ou version pdf <a href = "./Chapitres/C11_diviser_regner/ex_qui_tire/qui_tire_sur_qui_el.pdf"> Qui tire sur qui ? </a> </li>
<li> TP :  <a href ="./Chapitres/C11_diviser_regner/TP/C11_TP_div_reg.md"> md </a> ou <a href ="./Chapitres/C11_diviser_regner/TP/C11_TP_div_reg.pdf"> pdf </a>  </li> 
<ul>
</td>
</tr>

<tr>
<td>Révision</td>
<td ><span> Algorithme KNN et dictionnaires</span></td>

<td> <ul>
<li><a href="https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2FCecGhesq%2Ft_nsi_21/HEAD?labpath=Chapitres%2Frevisions%2Fknn_coqueluche_grippe%2FTP_KNN_ELEVE.ipynb">Sujet Notebook </a>en ligne </li>

<li> ou <a href="./Chapitres/revisions/knn_coqueluche_grippe/TP_knn_gri_coq_el.zip" >dossier à dézipper </a> et à ouvrir en console dans Thonny en se plaçant dans le <b>dossier</b> puis >>>jupyter notebook TP_KNN_ELEVE.ipynb </li>

<li> on peut aussi coller dans Thonny le code pour répondre aux questions</li>
</ul>
</td>

</tr>

<tr>
<td>Révision</td>
<td >Commandes bash</span></td>
<td>
<ul>
<li> TP Terminus   <a href ="./Chapitres/revisions/bash/TD_Jeu_Terminus.pdf"> pdf </a>  </li>
<li> Droits :  Résumé <a href ="./Chapitres/revisions/bash/diapo_droits.pdf"> pdf </a>  </li> 
</td>
</tr>
<tr>

<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-001.gif"></td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td>
 <b>Routage </b> </br>
<ul><li> Cours  <a href ="./Chapitres/C10_routage/C10_protocole_RIP.md"> md </a>  ou <a href ="./Chapitres/C10_routage/C10_protocole_RIP.pdf"> pdf </a>  </li>
<li> TP :  <a href ="./Chapitres/C10_routage/TP_routage_filius/TP_routage.md"> md </a> ou <a href ="./Chapitres/C10_routage/TP_routage_filius/TP_routage.pdf"> pdf </a>  </li> ; voici la table de routage et des éléments de réponse <a href ="./Chapitres/C10_routage/TP_routage_filius/table_routage.pdf"> table de routage </a>
<li> Exercices :  <a href ="./Chapitres/C10_routage/Exercices.md"> md </a> ou <a href ="./Chapitres/C10_routage/Exercices.pdf"> pdf </a> </li>
<li> Exercices type bac  <a href ="./Chapitres/C10_routage/sujets_bac/sujet_1_5.pdf"> Métropole 1 exercice 5 </a> et  <a href ="./Chapitres/C10_routage/sujets_bac/sujet_Sept2021_1.pdf"> Métropole septembre exercice 1 </a> et  <a href ="./Chapitres/C10_routage/sujets_bac/Sujet2_3_2021.pdf"> Métropole 2 exercice 3 </a> et enfin <a href = "./Chapitres/C10_routage/sujets_bac/Cent_Etran_1_4.pdf"> Centre étranger 1_4 </a></li> 
<li> Diaporama pour résumer le tout et réviser.... <a href = "./Chapitres/C10_routage/Transmission_de_donnees_dans_un_reseau.ppsx"> ici </a> </li>
</td>
</tr>

<tr>
<td style="color:#FF0000" > DS </td>
<td><span class="rouge" >C8_C9</span></td>
<td> <a href ="./DS/DS4.pdf"> sujet </a> et <a href ="./DS/DS4_correction.pdf"> correction </a>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-010.gif"> </td>
<td style="color:#09fa14" >Base de données</span></td>
<td>
 <b>Base de données relationnelles </b> </br>
<ul><li> Cours partie 1 <a href ="./Chapitres/C9_BDD/cours/C9_1.md"> md </a>  ou <a href ="./Chapitres/C9_BDD/cours/C9_1.pdf"> pdf </a>  </li>
<li> TP 1A :  <a href ="./Chapitres/C9_BDD/TP/C9_1A_TP.md"> md </a> ou <a href ="./Chapitres/C9_BDD/TP/C9_1A_TP.pdf"> pdf </a>  </li>
<li> TP 1B :  <a href ="./Chapitres/C9_BDD/TP/C9_1B_TP.md"> md </a> ou <a href ="./Chapitres/C9_BDD/TP/C9_1B_TP.pdf"> pdf </a>  </li>
<li> Cours partie 2 <a href ="./Chapitres/C9_BDD/cours/C9_2.md"> md </a>  ou <a href ="./Chapitres/C9_BDD/cours/C9_2.pdf"> pdf </a>  </li>
<li> TP 2 :  <a href ="./Chapitres/C9_BDD/TP/C9_2_TP.md"> md </a> ou <a href ="./Chapitres/C9_BDD/TP/C9_2_TP.pdf"> pdf </a> </li>
<li> Sujets de bac : [Amérique du Nord](./Chapitres/C9_BDD/exos_bac/Sujet_2021_Am_Nord_sql.pdf) [Métropole_1](./Chapitres/C9_BDD/exos_bac/sujet1_3_2021 sql.pdf)  [Métropole_2](./Chapitres/C9_BDD/exos_bac/sujet2_2021_SQL.pdf) 
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-009.gif"> </td>
<td style="color:#e06666" ><p>Structure de données</p>  <p> Algorithmique </p> </td>
<td><ul>
<li> <b>Les arbres </b>Cours partie 1 <a href ="./Chapitres/C8_arbres/cours/C8_1.md"> md </a>  ou <a href ="./Chapitres/C8_arbres/cours/C8_1.pdf"> pdf </a>  </li>
<a href ="https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2FCecGhesq%2Ft_nsi_21/HEAD?labpath=Chapitres%2FC8_arbres%2Fcours%2FC8_cours_arbres_binaires_partie_1_el.ipynb">Jupyter Notebook à compléter</a>
<li> TP 1 :  <a href ="./Chapitres/C8_arbres/TP/C8_TP1.md"> md </a> ou <a href ="./Chapitres/C8_arbres/TP/C8_1_TP.pdf"> pdf </a>  </li>
<li> Cours partie 2 <a href ="./Chapitres/C8_arbres/cours/C8_2_ABR.md"> md </a>  ou <a href ="./Chapitres/C8_arbres/cours/C8_2_ABR.pdf"> pdf </a>  </li>
<li> TP 2 :  <a href ="./Chapitres/C8_arbres/TP/C8_TP2.md"> md </a> ou <a href ="./Chapitres/C8_arbres/TP/C8_TP2_.pdf"> pdf </a>  </li>
</td>
</tr>
<tr>
<td style="color:#FF0000" > DS </td>
<td><span class="rouge" >C6_C7</span></td>
<td> <a href ="./DS/DS3.pdf"> sujet </a> et <a href ="./DS/DS3_corrigé.pdf"> correction </a>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-008.gif"> </td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td><ul>
<li> <b>Processus Ressources </b> <a href ="./Chapitres/C7_processus/7_processus_ressource.md"> md </a>  ou <a href ="./Chapitres/C7_processus/7_processus_ressource.pdf"> pdf </a></li>
<li> TP 1  <a href ="./Chapitres/C7_processus/TP/TP_C7_processus_def_el.md"> md </a>  ou <a href ="./Chapitres/C7_processus/TP/TP_C7_processus_def_el.pdf"> pdf </a> </li>
<li> TP 2  <a href ="./Chapitres/C7_processus/TP/TP2_C7_interblocage.md"> md </a>  ou <a href ="./Chapitres/C7_processus/TP/TP2_C7_interblocage.pdf"> pdf </a> </li>
<li style="color:#6aa84f"> sujet Bac 2021 sujet candidats libres  <a href ="./Chapitres/C7_processus/metro_2021_ex2_2.pdf"> : sujet </a>  </li>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-007.gif"> </td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td><ul>
<li> <b>Microcontrôleur et SoC </b> <a href ="./Chapitres/C6_microcontroleur_SoC/C6_microcontroleur_SoCs.md"> md </a>  ou <a href ="./Chapitres/C6_microcontroleur_SoC/C6_microcontroleur_SoCs.pdf"> pdf </a></li>
<li> TP  <a href ="./Chapitres/C6_microcontroleur_SoC/C6_TP_el.md"> md </a>  ou <a href ="./Chapitres/C6_microcontroleur_SoC/C6_TP_el.pdf"> pdf </a> </li>
<li style="color:#6aa84f"> sujet Bac 2021 Polynésie  <a href ="./Chapitres/C6_microcontroleur_SoC/Sujet_bac_polynésie_2021_ex 4.pdf"> : sujet </a>  </li>
</td>
</tr>
<tr>
<td style="color:#FF0000" > DS </td>
<td><span class="rouge" >C3_C4_C5</span></td>
<td> <a href ="./DS/DS2.pdf"> sujet </a> et <a href ="./DS/corrigé_DS2.pdf"> correction </a>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-006.gif"> </td>
<td style="color: #e06666">Structure de données</span></td>
<td><ul>
<li> <b>Pile File </b> <a href ="./Chapitres/C5_pile_file/C5_pile_file.md"> md </a>  ou <a href ="./Chapitres/C5_pile_file/C5_pile_file.pdf"> pdf </a></li>
<li> TP  <a href ="./Chapitres/C5_pile_file/exos/C5_TP_elève.md"> md </a>  ou <a href ="./Chapitres/C5_pile_file/exos/C5_TP_elève.pdf"> pdf </a> </li>
<li style="color:#6aa84f"> sujet Bac 2021 Amérique du Nord  <a href ="./Chapitres/C5_pile_file/exos/sujet_bac_amerique2021_ex5.pdf"> : sujet </a>  </li>
<li style="color:#c27ba0" > sujet epreuve pratique sujet_6 ex2 : attention correction du sujet initial <a href="./ECE/21_NSI_06_corr.py"> sujet modifié </a> </li>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-005.gif"> </td>
<td style="color: #e06666" >Structure de données</span></td>
<td><ul>
<li> <b>Liste chainée </b><a href ="./Chapitres/C4_liste_chainee/C4_liste_chainee.md"> md </a>  ou <a href ="./Chapitres/C4_liste_chainee/C4_liste_chainee.pdf"> pdf </a></li>
<li> TP  <a href ="./Chapitres/C4_liste_chainee/exos/C4_TP.md"> md </a>  ou <a href ="./Chapitres/C4_liste_chainee/exos/C4_TP.pdf"> pdf </a> </li>

</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-004.gif"> </td>
<td style="color: #e06666" >Structure de données</span></td>
<td><ul>
<li> <b>La programation orientée objet </b><a href ="./Chapitres/C3_POO/C3_PPO_cours.md"> md </a>  ou <a href ="./Chapitres/C3_PPO/C3_poo_cours.pdf"> pdf </a></li>
<li> TP POO <a href ="./Chapitres/C3_POO/tp/C3_TP_POO_el.md"> md </a>  ou <a href ="./Chapitres/C3_POO/tp/C3_poo_tp.pdf"> pdf </a> </li>
<li style="color:#6aa84f"> Sujet de bac <a href ="./Chapitres/C3_POO/tp/jeu_de_la_vie/Bac_Sujet2_Exercice_1.pdf"> pdf </a> </li>
<li> TP Jeu de la vie <a href ="./Chapitres/C3_POO/tp/jeu_de_la_vie/C3_TD_jeu_de_la_vie.md"> md </a>  ou <a href ="./Chapitres/C3_POO/tp/jeu_de_la_vie/C3_TD_jeu_de_la_vie.pdf"> pdf </a> <a href ="./Chapitres/C3_POO/tp/jeu_de_la_vie/jeudelavie_correction.pdf"> CORRECTION : </a>  </li>
</td>
</tr>

<tr>
<td style="color:#FF0000">DS </td>
<td><span class="rouge" >C1_C2</span></td>
<td> <a href ="./DS/DS1.pdf"> sujet </a> et <a href ="./DS/Correction_DS1.pdf"> correction </a>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-003.gif"> </td>
<td style="color:#17657D" >Langages, programmation et algorithmique</span></td>
<td><ul>
<li><b>La récursivité </b><a href ="./Chapitres/C2_Recursivite/C2_Recursivite_cours.md"> md </a> </li>
<li> notions mathématiques <a href ="./Chapitres/C2_Recursivite/tp/2_recurrence_math_el.md"> md </a>  </li>
<li> TP Récursivité <a href ="./Chapitres/C2_Recursivite/tp/2_TP_recursivite_el.md"> md </a>  </li>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"> </td>
<td style="color:#17657D" >Langages, programmation et algorithmique</span></td>
<td><ul>
<li> <b>Modularité Interface </b><a href ="./Chapitres/C1_modularite/C1_modularite.md"> md </a> </li>
<li> TP 1 modules<a href ="./Chapitres/C1_modularite/TP/TP_modularité_eleve.md"> md </a>  </li>
<li> TP 2 interface <a href ="./Chapitres/C1_modularite/TP/TP2_interface/C1_TP_interface.md"> md </a>  </li>
</td>
</tr>

<tr><td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-001.gif"></td><td>révision</td><td>boucle bornée, non bornée, comparaisons, listes, tuples, dictionnaires,.....
<a href ="./Chapitres/revisions/revisions.md"> lien vers la feuille de travail </a>  et <a href ="./Chapitres/revisions/revisions.py"> le corrigé </a> 
</td>
</tr>

<tr><td>1_NSI</td><td></td><td><a href="https://framagit.org/CecGhesq/1_nsi_21_mdf"> git_premiere_nsi </a></td></tr>
</table>


