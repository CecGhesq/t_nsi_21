---
title: "Structure de données : 3_Programmation orientée objet"
subtitle: "TP _POO"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
# Exercice 1 : QCM &#x1F3C6;

1. On définit la classe `Identite` de la manière suivante : 

```python
class Identite:
    def __init__(self, nom, prenom, an):
        self.nom = nom
        self.prenom = prenom
        self.an = an
    
    def age(self, a):
        return a - self.an
>>> hugo = Identite('Hugo', 'Dupont', 1999)
>>>print(hugo.age(2020))
```

Que s'affiche-t-il?  
[ ] 'Hugo'  
[ ] 'Dupont'  
[ ] 1999  
[ ] 21

2. On définit la classe `Voiture` de la manière suivante

```python
class Voiture:
    def __init__(self, nom, couleur, puissance):
        self.nom = nom
        self.couleur = couleur
        self.puissance = puissance
    
    def nc(self):
        return nom + couleur

>>> F430 = Voiture('Ferrari', 'Rouge', 43)
>>> print(F430.nc())
```

Que s'affiche-t-il?  
[ ] 'Ferrari Rouge'  
[ ] 'Rouge Ferrari'  
[ ] une erreur  Name Error  
[ ] 43

# Exercice 2 : Ecrire une classe &#x1F3C6;

_Pour chacune des questions suivantes, un constructeur initialisant les mesures devra être présent dans chacune des classes._  

|||
|:---|:---|
|1. Ecrire une classe nommée `Carre` admettant la mesure des côtés d'un carré en attribut, avec deux méthodes :  |![](./img/ex2_s.jpg)|
|une méthode `perimetre` permettant de retourner le périmètre du carré ;   |une méthode `aire` permettant de retourner son aire.|  

Faire apparaître la structure avec l'onglet "Affichage" de Thonny
  
2. Ecrire une classe nommée `Triangle` représentant un triangle et admettant la mesure des trois côtés comme attributs, avec deux méthodes :  

* une methode `aire` renvoyant l'aire du triangle à l'aide de la formule de Héron : $`A = \sqrt{p(p-a)(p-b)(p-c)}`$ où  a, b, c sont  les longueurs \: des  trois  côtés  et  où $`p  = \frac{a + b + c}{2}`$  

* une méthode `rect` qui renvoie `True` si le triangle est rectangle, et `False` sinon.

3. Définir alors un carré de côté 5, puis afficher son aire et son périmètre.  
Définir ensuite un triangle de côté 5.4,9 et 7.2 pour affichier son aire et regarder si il est rectangle.  

# Exercice 3 : La classe Temps &#x1F3C6;&#x1F3C6;  
En Python, écrire une classe `Temps` qui permet de définir un horaire au format hh:mm:ss ( 3 attributs h,m, ets) et qui admet les méthodes suivantes :  

* `affiche` qui affiche l'horaire au format " 12 h 37 min 47 s " ; 
* `__add__`qui ajoute deux horaires de la classe Temps ; 
* `__sub__` qui calcule la différence entre deux horaires de la classe `Temps`.

# Exercice 4 : La classe Angle &#x1F3C6;&#x1F3C6;  

On souhaite définir une classe `Angle` pour représenter un angle en degrés. Cette classe contient un unique attribut, `angle`, qui est un entier. On demande que, quoiqu'il arrive, l'égalité $`0 \leq angle < 360`$ reste vérifiée.  

1. Ecrire le constructeur de cette classe.
2. Ajouter une méthode __ __str__ __ qui renvoie une chaîne de caractères de la forme "60 degrés". Observer son effet en construisant un objet de la classe `Angle` puis en l'affichant avec __print__.
3. Ajouter une méthode `ajoute` qui reçoit un autre angle en argument ( un objet de la classe `Angle`) et l'ajoute au champ `angle` de l'objet. Attention à ce que la valeur d'angle reste bien dans le bon intervalle.  
4. Ajouter deux méthodes `cos` et `sin` pour calculer respectivement le cosinus et le sinus de l'angle. On utilisera pour cela les fonctions `cos` et `sin`de la bibliothèque `math`. ( Attention il faudra convertir l'angle en radians).  
5. Tester les méthodes `ajoute`, `cos` et `sin`.  

# Exercice 5 : Tableaux indexés au choix &#x1F3C6;&#x1F3C6;  
Dans certains langages de programmation, les tableaux ne sont pas nécessairement indexés à partir de 0.   
On se propose dans cet exercice de construire une classe Tableau pour réaliser de tels tableaux. Un objet de cette classe aura deux attributs, un attribut `premier` qui est la valeur du premier indice et un attribut `contenu` qui est un tableau Python contenant les éléments. Ce dernier est un tableau indexé à partir de 0.

1. Ecrire le constructeur __init__(self, imin, imax, v) où __imin__ est le premier indice, __imax__ est le dernier indice, et __v__ la valeur utilisée pour initier toutes les cases du tableau.  
t = Tableau(-10,9,42) est un tableau de 20 cases indexées de -10 à 9 toutes initialisées avec la valeur 42.  
2. Ecrire une méthode __ __len__ __(self)  qui renvoie la taille du tableau.  
3. On va utiliser l'indice `i` dans le tableau : vérifier que l'indice i est bien valide et, dans le cas contraire, lever l'exception `IndexError` avec la valeur i en argument ( `raise` __IndexError(i)__). On écrira ainsi une méthode `__ __indice_valide__(self , i)__ permettant de vérifier cette condition.
4. Ecrire une méthode __ __getitem__ __ (self , i) qui renvoie l'élément du tableau __self__ d'indice `i`. De même, écrire une méthode __ __setitem__ __ (self , i, v) qui modifie l'élément du tableau `self` d'indice `i` pour lui donner la valeur v. On exploitera la méthode validant la valeur de l'indice i précédente.
5. Ecrire une méthode __ __str__ __(self) qui renvoie une chaîne de caractères décrivant le contenu du tableau.



Sources :

* NSI Terminale Nathan les vrais exos S Pasquet M Leopoldoff  
_________________________
 
