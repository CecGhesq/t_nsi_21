class Personne :
    def __init__(self , birthyear , tall , weight) :
        self.birthyear = birthyear
        self.tall = tall
        self.weight = weight
        
    def grandir(self , h) :
        self.tall = self.tall + h
        
luc = Personne(2002 , 178 , 69)
init_tall = luc.tall
luc.grandir(8)
new_tall = luc.tall

print("Luc pèse ", luc.weight , "kg.")
print("Muc mesure à présent" , new_tall , "cm après avoir grandi de 8 cm en 1 an. Avant ça, il mesurait : " , init_tall, "cm")

