---
title: " 13 Les graphes "
subtitle: "TD : l'ours en cage "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

On dispose d’une liste de mots ayant le même nombre de lettres.

__Comment à partir d’un mot choisi dans cette liste, aboutir à un second mot de cette liste, sachant que le chemin parcouru sera réalisé en naviguant de mot en mot et que deux mots consécutifs de la suite ne diffèrent que d’une lettre, sans tenir compte de l’ordre des lettres dans chacun des mots ?__

Voici un exemple avec des mots de quatre lettres (fichier [liste_mots.py](liste_mots.py) ) 


Téléchargez le dossier __nsi_ours_en_cage__, sous Thonny éditez le script Python nsi_ours_en_cage.py, ainsi que le fichier liste_mots.py contenant la liste des mots nécessaires à notre activité.

# Liste des mots n’ayant qu’une lettre différente dans leur écriture

Le tracé du graphe sera réalisé à partir de la liste d’adjacence.
Pour chacun des mots de la liste, un mot est voisin d’un autre __s’il ne diffère de l’autre que d’une lettre__ pour son écriture.

1. Codez la fonction distance fournissant le nombre de lettres qui diffèrent d’un mot à l’autre.  

```python
def distance(mot_1, mot_2):
    '''
    Renvoie le nombre de lettres apparaissant dans le mot_1 qui ne sont pas 
    présente dans le mot_2
    Si une lettre apparaît deux fois dans le mot_1, elle devra apparaitre 
    2 fois dans le mot_2 pour qu'il y ait une différence nulle, 'aabc' 
    et 'abcd' ont une différence de 1 lettre
    :param: (str) chaine de caractères
    :return: (int) nombre de lettres du mot_1 n'apparaissant pas dans le mot_2
    :CU: mot_1 et mot_2 sont des chaines de caractères non nulles, de même longueur
    
    >>> distance("abcd", "abcd")
    0
    >>> distance("abcd", "dcba")
    0
    >>> distance("abcd", "1abc")
    1
    >>> distance("abcd", "a12c")
    2
    >>> distance("abcd", "d123")
    3
    >>> distance("abcd", "aaaa")
    3
    >>> distance("abcd", "1234")
    4
    
    '''
```

2. Coder la fonction `liste_mots_voisins` retournant pour un mot, la liste des mots n’ayant qu’une lettre différente dans leur écriture.

```python
def liste_mots_voisins(liste_mots, mot_source):
    '''
    Retourne une liste de tous les mots contenus dans la liste
    passée en paramètre dont une lettre diffère du mot passé 
    en paramètre 
    :param: (list of str) liste de mots de 4 lettres
    :param: (str) mot de caractères
    :return: (list of str) liste contenant tous les mots de distance =1
    :CU: la liste existe, les mots ont tous une longueur de 4 caractères
    
    >>> liste_mots_voisins(LISTE_MOTS, 'ours')
    ['duos', 'durs', 'fors', 'hors', 'mors', 'murs', 'purs', 'sure', 'tors']
    
    >>> liste_mots_voisins(LISTE_MOTS, 'cire')
    ['brie', 'cime', 'cris', 'cure', 'dire', 'lire', 'mire', 'pire', \
    'prie', 'raie', 'rime', 'rire', 'scie', 'trie']
    
    >>> liste_mots_voisins(LISTE_MOTS, 'LEGT')
    []

    '''
```

3. En déduire la fonction fournissant la `liste d’adjacence` présentée sous la forme d’un dictionnaire.

```python
def liste_adjacence(liste_mots):
    """
    Retourne le dictionnaire de la liste des mots liés pour chaque mot de la 
    liste de mots
    {'aime': [], 'sure': ['jure'], 'jure': ['sure'], 'brie': []}
    :param: (list of str) liste de mots de 4 lettres
    :return: (dict of list) dictionnaire contenant pour chaque mot la liste
    des mots voisins
    :CU
    
    >>> LISTE_MOTS = ["aime", "sure", "jure", "brie"]
    >>> liste = liste_adjacence(LISTE_MOTS)
    >>> sorted(liste.items())
    [('aime', []), ('brie', []), ('jure', ['sure']), ('sure', ['jure'])]
    
    """
```

4. On pourra alors vérifier en utilisant la fonction `affichage_liste_adjacence` les listes obtenues à partir des listes de mots LISTE_MOTS_0, LISTE_MOTS_1 et LISTE_MOTS_2.

```python
# Programme principal
print(affichage_liste_adjacence(liste_adjacence(LISTE_MOTS_0)))

>>> %Run nsi_ours_en_cage.py
aime --> 
sure --> jure 
jure --> sure 
brie --> 
```

5. Tracer le graphe pour la LISTE_MOTS_1 

```python
# Tracé du graphe
graphe_mots = graphe(liste_adjacence(LISTE_MOTS_1))
nx.draw(graphe_mots, with_labels = True ,  \ 
node_size = 1000 , node_color = 'lightgrey' , arrowsize = 10)

# Affichage à l'aide de matplotlib
plt.show()
```

![](./img/graphe.jpg)  

![](./img/graphe_zoom.jpg)  

On pourra __modifier les paramètres__ de la fenêtre de matplotlib afin d'observer le graphe au mieux ou modifier les valeurs des arguments de la fonction `draw()`.

# Recherche du chemin d’un mot à l’autre par un parcours en profondeur :

6. Explicitez ce parcours, c’est-à-dire l’algorithme que vous pourriez mettre en œuvre pour qu’à partir d’un mot_source aboutir à un mot_destination sans passer deux fois par le même mot et en enregistrant la liste des mots de votre parcours.

```




```

7. Pourquoi appliquer la récursivité pour ce parcours en profondeur, vous semble adapté ?

```




```

8. Dans le cas d’une fonction récursive quelles seront les conditions de sortie de la fonction récursive ?

```




```

9. Quels paramètres de la fonction récursive vous semblent nécessaires pour retrouver les nœuds permettant d’aller du nœud source au nœud destination et ne pas repasser sur un nœud ?

```


```

10. Proposez alors la fonction récursive permettant à partir de la liste des mots, d’un mot source et d’un mot destination, __d'afficher__ la liste des mots permettant le passage du mot source au mot destination.

```python
>>> dfs(list_adj , 'ours', 'cage', ['ours'], dico_visite)  
['ours', 'hors', 'sure', 'bure', 'brie', 'baie', 'haie', 'laie', 'cale', 'cage']
```
