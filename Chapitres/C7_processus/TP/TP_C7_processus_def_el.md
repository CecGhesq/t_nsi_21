---
title: "Architecture matérielle : __7 Processus resssources__"
subtitle: "TP 1 processus"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Processus

1. Afin de faciliter les manipulations de cet exercice, il est recommandé d’ouvrir deux consoles l'une au-dessus de l'autre sur votre écran. nous nommerons celle du haut __terminal_etat__ et celle du bas __terminal_execution__. Renommer les avec `CTRL + MAJ + I`

## PID et relation entre les processus  

2. Dans __terminal_etat__ utiliser la commande `ps` pour obtenir le PID du terminal et les enfants du terminal. Noter et décrire le résultat. Quel est le PID du terminal ?

```


```

Faire de même dans __terminal_execution__.  

```


```

3. Qu'obtient-on avec la commande `ps -l` ?

```


```
## Observation statique avec ps
4. Dans terminal_etat entrer la commande `pstree` puis la commande `pstree -p`. Comparer les deux résultats.
 
```


```

5. Dans l’arbre des processus identifier l’arborescence locale aux deux terminaux. Qui est leur processus parent ? Quel est son nom et son PID ?

```


```

6. Recopier et décrire l’arborescence à partir de leur parent.

```


```

7. Montrer que _systemd_ est le parent de tous les processus en cours sur la machine.




## Observation dynamique avec top

La commande top permet d’observer en temps-réel les processus sur votre ordinateur à l’instar du gestionnaire de tâches sur Windows. Lancez cette commande dans __terminal_etat__.  

```bash
$ top
```
![top](./img/top_an.jpg)

On peut aussi utiliser `top -d 0.5`qui permet de changer le délai de rafraichissement.  

8. Notez les différentes informations présentées ci-dessus pour votre ordinateur. On peut quitter "top" en tapant `q`. 

9. La commande `find` permet de rechercher des fichiers sur le système d’exploitation.  
Lancez une recherche dans le __terminal_execution__ de tous les scripts Python présents depuis la racine du système de fichiers avec :

```bash
$ find / -name "*.py"
```

Observez dans le  __terminal_execution__ l’apparition du processus lié à la commande find, son PID, son état, son utilisation du CPU, de la mémoire...
Vous avez changez d’avis, tuer le processus « find » à partir de son PID en utilisant soit :  

* dans top, en appuyant sur [k] et en notant le PID
* dans le __terminal d'execution__ avec [CTRL]+[C] qui envoie le signal SIGINT (veuillez vous interrompre s’il vous plaît)

## Etat des processus et consommation des ressources

10. Lancez la calculatrice depuis le menu de démarrage Raspberry/Accessoires/Calculatrice (Galculator) et Libre Office Writer  Bureautique/LibreOffice Writer.  

11. Afficher la liste de tous les processus de manière statique est `ps`. Dans le __terminal_execution__ :

```bash
$ ps -aux
```

12. Donner les états dans lesquels se trouvent les processus Galculator et Libre Office Writer.


```


```

13. On peut également opérer un filtrage (commande grep) en utilisant le caractère « pipe » ([ALT GR]+[6]) entre 2 commandes. Ce caractère agit comme un tuyau entre la sortie stdout de l’un et l’entrée stdin de l’autre.  

```bash
$ ps -aux | grep galc*
```

![pipe](./img/pipe.jpg)

Retrouvez le processus lié à la calculatrice lancé précédemment. Notez le PID.

14. Afficher uniquement la partie de l'arborescence de Libre Office à partir du parent dans le __terminal_etat__ avec la commande `pstree -p ` n° PPID.

```
$ pstree -p 1544 # n° PPID
```

15. Retrouvez également le processus « galculatrice » et retracer sa descendance jusqu’au processus initial (PID=1)
Notez les accolades dans l’affichage qui représentent les threads (processus légers). Il devrait y en avoir quelques unes.  

16. Tuez le processus « galculatrice » avec la commande kill qui envoie le signal SIGKILL à un numéro de PID de processus (ici XXXX)

```bash
$ kill XXXX
```
Vérifier avec `pstree` par exemple que cela a bien disparu.  

17. Lancez la commande sleep qui comme son nom l’indique permet d’attendre en dormant. Vous comparerez les 2 lancements suivants dans le __terminal d'execution__ :

```bash
$ sleep 10
$ sleep 10 &
```

Voici un résumé des commandes observées :  

|Commande|	Description|
|:---|:---|
|ps|Affiche des informations sur le processus dans lequel est lancé la commande et ses enfants.|
|ps -aux|Affiche des informations sur tous les processus en cours d’exécution sur la machine|
|ps -l|Affichage long. Donne notamment le PPID du parent de chaque processus.|
|pstree| Affiche l’arbre des processus en cours d’exécution sur la machine|
|pstree -p|	Ajoute les PID de chaque processus|
|pstree -p 1712|Affiche l’arbre des processus à partir du PID 1712|
|top|Affiche des informations détaillées sur tous les processus en cours d’exécution en temps réel.|
|top -d 0.5|Permet de changer le délai de rafraîchissement des informations.|
|top -p 1712|Affiche des informations détaillées uniquement sur le processus 1712.|
|top -p 1712,1713|Affiche des informations détaillées sur le processus 1712 et 1713.|
|commande &|Permet à l’utilisateur de reprendre la main après l’exécution d’une commande.|
|Commande_1 & commande_2|Lance deux commandes l’une à la suite de l’autre.|
|Commande_1 & commande_2 &|Idem en rendant la main à l’utilisateur.|
|kill 1712|Termine le processus 1712|
|kill -SIGSTOP 1712|Envoie le signal SIGSTOP au processus 1712|



# Suivre des processus avec Python

Dans cet exercice vous allez utiliser un script Python qui lancera de différentes manières des processus. Un script  vous est proposé  : 


Il s’agit au travers d’un menu, de créer et lancer des processus.   
Voici le menu :

![](./img/simul_proc.jpg)

1. Compléter le tableau suivant avec les méthodes ou fonction utilisées en détaillant l'utilisation (enregistrer votre fichier dans vos Documents et lancez-le):  

|Menu| Description|
|:---|:---|
|1|
|2||
|3||
|4||
|5||
|6||
|7||
|8||
|9||
|666||
|0||

2. Toujours en utilisant 2 terminaux l'un sur l'autre vous allez observer le comportement du programme.  
Lancez la commande top dans le __terminal_etat__ et votre programme dans le __terminal_execution__ (en __se plaçant préalablement dans le dossier__ dans lequel vous avez mis le fichier(commande ls cd ....)).  

```bash
$ top
```

```bash
$ python3 simulateur_processus.py
```

* __Entrée 1__ :

    * Affichez le PID et le PPID de votre programme.  
    * Retrouvez votre programme avec la touche de recherche [L] dans top. Vous pourrez saisir « python3 » ou le PID de votre programme.   L’affichage de la commande complète avec la touche [c] vous permettra de mieux visualiser votre processus.  
    * Sortir de votre programme et le relancer. Affichez à nouveau le PID et le PPID. Est-ce que les valeurs ont changé depuis le premier lancement ? A quel processus correspond le PPID ?  
    * Affichez les informations de votre processus dans dans le __terminal_etat__  avec :  

```bash
$ sudo cat /proc/XXXX/status
```

* __Entrée 2 du menu__ :

    * Afficher les variables d’environnement et retrouvez les valeurs de USER, PATH et LANG.  
    * A quoi sert la variable d’environnement PATH ? Faire une recherche si besoin.


* __Entrée 3 du menu__ :  
    Vérifiez que le résultat est correct.


* __Entrée 4 du menu__ :
    Vérifiez que le résultat est correct.  


* __Entrée 5 du menu__ :  
    Normalement votre menu doit être utilisable en parallèle de la calculatrice, car cette dernière a été lancée en tâche de fond.


* __Entrée 6 du menu__ :  
    Vérifiez que le résultat est correct.


* __Entrée 7 du menu__ :  
    * Dans top, affichez le détails de chaque coeur du CPU avec la touche [1]  : observez les %Cpu pour les différents coeurs.
    * Notez le temps d’exécution des calculs


* __Entrée 8 du menu__ :  
    * Vérifiez à l’aide des PID et PPID la filiation entre les processus père et fils.
    * Notez le temps d’exécution des calculs. Il devrait être similaire à l'entrée 7.

* __Entrée 9 du menu__ :
    * Observez le travail des différents coeurs dans le __terminal_etat__
    * Notez le temps d’exécution des calculs. Il devrait être plus faible que pour 7 et 8 car vous avez parallélisé le calcul en le découpant en 3 parties chacune exécutée en parallèle sur un coeur.

Tous les algorithmes ne sont pas parallélisables, mais quand ils le sont ils font largement partie des CPU multi-coeurs.

* __Entrée 666 du menu__ :
Dans top, les processus devraient apparaître  avec un Z comme zombie dans la colonne état. (Faire éventuellement une recherche avec top -p XXX pour suivre le processus)
Comme tout zombie qui se respecte, il n’est pas possible de les tuer directement (ils sont déjà morts). Essayez avec la touche [k] dans top.
Ils disparaîtront quand vous terminerez votre programme ou lorsque que le système d’exploitation les aura adoptés (pour les tuer et libérer les ressources utilisées).

# Ordonnancement

## Introduction
Soit les tâches (=processus) suivantes à exécuter sur une machine ne disposant que d'un processeur (on négligera le temps de commutation de contexte) et selon certaines caractéristiques :  
|Tâche| Durée |Priorité |Date début|
|---|---|---|---|
|T1 |4s |Basse| 0s|
|T2| 7s| Moyenne| 3s|
|T3| 3s| Haute |7s|  

## Cas 1

Tracez le chronogramme d’ordonnancement sachant que :

* algorithme d’ordonnancement = FIFO collaboratif
* on ne tient pas compte des priorités  

![cas](./img/cas.jpg)

Que pensez-vous de la situation à la fin ?  

* avantages :

```

```

* inconvénients :  


```

```

## Cas 2  

Tracez le chronogramme d’ordonnancement sachant que :  

* algorithme d’ordonnancement = FIFO préemptif  
* on tient compte des priorités  

![cas](./img/cas.jpg)


Que pensez-vous de la situation à la fin ?  

* avantages :

```

```

* inconvénients :  


```

```

## Cas 3  

Tracez le chronogramme d’ordonnancement sachant que :  

* algorithme d’ordonnancement = Round Robin préemptif
* on ne tient pas compte des priorités
* quantum de temps = 1s

![cas](./img/cas.jpg)


Que pensez-vous de la situation à la fin ?  

* avantages :

```

```

* inconvénients :  


```

```

# Cas 4  

Tracez le chronogramme d’ordonnancement sachant que :
* algorithme d’ordonnancement = Round Robin préemptif
* on tient compte des priorités
* quantum de temps = 1s
* la tâche T2 se retrouve bloquée à t=4s pendant 1s


![cas](./img/cas.jpg)


* avantages :

```

```

* inconvénients :  


```

``` 

Comment faudrait-il modifier les priorités pour s’assurer que la tâche T2 se termine à t=12s au maximum ?



```

``` 
Sources :  

* cours Niort lycée Saint André A MAROT D SALLÉ J SIMONNEAU  

* Cours TNSI S Ramstein lycée Queneau Villeneuve d'Ascq  

