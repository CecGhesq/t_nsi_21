---
title: " 15 Sécurisation des communications"
subtitle: "Architecture matérielle : Envoi de fichiers signés et chiffrés "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
# Intégrité d'un fichier

La société Softwhere propose différentes solutions logicielles à télécharger sur son  [site](https://www.thelittlewoodstudio.fr/nsi/softwhere/)

![](./img/softwhere.jpg)

Afin de garantir l’intégrité des fichiers à télécharger elle a créé et mis à disposition les empreintes SHA256 comme expliqué dans l'exemple ci_dessous : 

![](./img/signature_num.jpg)

__Retrouvez le fichier problématique parmi les 3 téléchargements proposés sur le site Web.__  

Dans un dossier, télécharger les fichiers zippés puis utiliser, dans ce dossier, le PowerShell. La commande suivante permettra de voir la signature du document. Comparez-là avec celle annoncée sur le site pour conclure.

```bash 
PS H:\> certUtil -hashfile file.zip SHA256
```
Pour connaitre la syntaxe et les possibilités de cette commande en ligne Windows on peut utiliser : `certUtil -hashfile -?`

Remarque : sur un bash de type Unix, la commande est : 
```bash
$ sha256sum le_fichier.zip
```

# Signature numérique  
Nous allons montrer comment on peut utiliser `open SSL` pour réaliser un chiffrage avec clé publique.  
Dans ce scénario Alice et Bob vont générer leur clé publique et privée en utilisant l’algorithme RSA.  
Alice va ensuite prendre un message, signer son message, créer une signature digitale puis enfin chiffrer le message avec la clé public de Bob.
Bob va déchiffrer le message avec sa clé privée, et vérifier la signature pour s’assurer qu’Alice en est bien l’auteur.
* Créer un dossier `signature_num_` dans lequel vous ouvrer le powershell  

* Vérifier l'installation  de `openssl` : 
```bash
> openssl version
```



## Générer les clés  

### Générer la clé privée pour un utilisateur A  

* Générer la clé privée d'Alice
```bash
> openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -pkeyopt 
 rsa_keygen_pubexp:3 -out prk_userA.pem
``` 
_Commentaires:_

`genpkey` : generate a private key  
`-algorithm` : nom de l’algorithme utilisé RSA  
`pkeyopt` : Option pour la clé privée (voir aide genpk)  
`rsa_keygen_bits` :2048 (modulo de la clé ici 2048 bits pour n)  
`rsa_keygen_pubexp` :3 (exposant de la clé public pour e)  
`-out` pour générer un fichier nommé prk_userA.pem 

* Ouvrir le fichier dans Notepadd++ : on observe la suite d'octets ! 
* On peut voir la clé avecla commande :
```bash
> openssl pkey -in prk_userA.pem -text
```


_Commentaire_:
`pkey` : lire une clé private key  
`-in` : nom du fichier contenant la clé  
`-text` : pour obtenir un rendu text  

![](./img/chiffrage.jpg)  
On voit bien que la __clé publique est une partie de la clé privée__.

|||
|---|---|
|![](./img/modulus.jpg)|![](./img/exponent.jpg)|
|__modulus__ : Les 2048 bits de n : Soit 2048/8 256octets la clé en affiche 257 mais le premier est à 0|__publicExponent__ : 3 La valeur de notre choix 3 pour l’exposant de la clé publique « e » ; __privateExponent__ : L’exposant pour la clé privée « d » ici sur 256 octets|
|![](./img/prime.jpg)|![](./img/exponent2.jpg)|
|__prime1 et prime 2__ : Les deux nombres premiers utilisés pour générer le n du modulo|On peut observer trois informations supplémentaires __exponent1, exponent2, coefficient__. Leur valeur est utilisée pour accélérer le déchiffrage de la clé privée.|

### Générer la clé publique pour un utilisateur A  

* Génerer la clé publique d'Alice  

```bash 
> openssl pkey -in prk_userA.pem -out puk_userA.pem -pubout
```

_Commentaire_: 
`pkey` : lire une clé private key  
`-in` permet de préciser le nom du fichier de clé privée  
`-out` permet de préciser le nom du fichier de clé publique  
`-pubout` pour générer la clé publique  

* Observer le résultat en ouvrant le fichier prk_userA.pem.  
* On peut voir la clé avec la commande:

```bash
> openssl pkey -in puk_userA.pem -pubin -text
```
On retrouve le modulo n et l'exposant e dans la clé publique.


### Générer la clé privée et publique  pour un utilisateur B  

* Générer les clés pour Bob : 

```bash
> openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -pkeyopt 
rsa_keygen_pubexp:3 -out prk_userB.pem
> openssl pkey -in prk_userB.pem -out puk_userB.pem -pubout
```
|||
|---|---|
|On dispose maintenant des 4 clés, qui en réalité ne sont pas sur la même machine ! $`\mathrm{K}_{pr}^{A} \,; \, \mathrm{K}_{pu}^{A} \, et \,  \mathrm{K}_{pr}^{B} \,; \, \mathrm{K}_{pu}^{B} \,`$ |![](./img/explo.jpg)  |

## Réaliser une signature numérique  

* Créer un fichier msg_clair.txt : 

![](./img/mess_clair.jpg)  

Pour assurer une meilleure sécurité on doit signer le message à l’aide d’une __signature numérique__.
* Créer une __table de hash__ du message avec la commande (-sha1 pour choisir l’algorithme de hash) : 

```bash
> openssl dgst -sha1 msg_clair.txt
```
* Chiffrer cette signature par la clé privée de Alice :

```bash
openssl dgst -sha1 -sign prk_userA.pem -out signat_userA.bin msg_clair.txt
```
_Commentaires_:  
`dgst` : digest functions  
`-sign` pour signer  

* Observer dans votre dossier l'apparition de ce nouveau fichier.  

## Alice envoie le message chiffré et signé  

Alice veut maintenant transmettre le message. Mais il faut le chiffrer. _On utilise ici habituellement un algorithme symétrique qui est plus rapide_. Pour la démonstration nous continuions avec RSA et la clé publique de Bob.  

On utilise la commande pkeyutl avec l’option -encrypt

```bash
> openssl pkeyutl –encrypt -in msg_clair.txt -pubin -inkey puk_userB.pem -out
 msg_chif.bin
```

## Bob déchiffre le message  
Lorsque Bob reçoit le message (msg_chif.bin ) il doit déchiffrer le message pour retrouver l’original (msg_recu.txt) puis vérifier sa signature.  

* Déchiffrer le message d'Alice :

```bash
openssl pkeyutl –decrypt -in msg_chif.bin -inkey prk_userB.pem -out msg_recu.txt 
```

Le fichier msg_recu.txt est décodé correctement !

Pour être certain que le message n’ait pas été modifié,  Bob doit vérifier sa signature (il doit connaitre l’algorithme de hash de la signature):  

* Vérifier donc la signature :
```bash
openssl dgst -sha1 -verify puk_userA.pem -signature signat_userA.bin msg_recu.txt
```
```
Verified OK
```

* Utiliser le dossier commun pour vous échanger les clés publiques et les messages signés et chiffrés pour ressayer le fonctionnement !!!

__Sources__ : 

* Cours NSI David Salle ( Niort)  
* Cours NSI Luc Vincent ( Académie de Bordeaux)  
