---
title: "Structure de données : 8_ Arbres binaires"
subtitle: "cours partie 2 : arbre binaire de recherche"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

![BO](./img/BO2.jpg)

# Arbre binaire de recherche (ABR)

Un **arbre binaire  de recherche**, ABR, est un arbre binaire qui va être utilisé pour réaliser « efficacement » des opérations de recherche d'une valeur, mais aussi des opérations d'insertion et suppression de valeurs.

Les valeurs des étiquettes de l'arbre doivent donc appartenir à un ensemble ordonné.

>📢 Définition :
Un ABR a pour tout noeud d'étiquette `e` :  
>- les étiquettes de tous les noeuds de son sous-arbre gauche sont inférieures ou égales à `e`
>- les étiquettes de tous les noeuds de son sous-arbre droit sont supérieures à `e`.

![abr](./img/abr.jpg)  

Les fonctions vues dans C8_1 sont toujours valables à savoir la `taille`, la `hauteur` et le `parcours_infixe`.

# Recherche dans un ABR  

## Algorithme
La propriété d'ABR tire tout son intérêt de l'efficacité de la recherche qu'elle permet. En effet, pour chercher une valeur dans un ABR, il suffit de la comparer à la valeur à la racine puis, si elle est différente, de se diriger vers un seul des deux sous-arbres. On élimine ainsi complètement la recherche sans l'autre sous-arbre.

On écrit ainsi la fonction appartient(x,abr) :  

* si l'arbre est vide, la réponse est évidente  
* si la valeur x est inférieure à la valeur du noeud, on continue la recherche à gauche
* si la valeur x est supérieure, on explore à droite
* si la valeur x est égale à la valeur du noeud, on renvoie True

```python
def appartient(x,abr):
    '''détermine si x apparaît dans l'ABR
    '''
    if abr is None : 
        return False
    if x < abr.valeur:
        return appartient(x,abr.gauche)
    elif x > abr.valeur:
        return appartient(x,abr.droite)
    else : 
        return True
```

## Complexité de l'algorithme de recherche dans un arbre binaire de recherche équilibré

La complexité en temps d'un algorithme est l'évaluation du nombre d'opérations élémentaires qu'il va effectuer pour obtenir le résultat attendu. Cette estimation ne pouvant être exacte et dépendant trop des conditions matérielles et logicielles d'exécution de l'algorithme, seule l'évolution de la complexité en fonction du nombre de données `n` à traiter donne une information pertinente.

![complexite](./img/complexite_graph.png)

Dans le cas de l'algorithme de recherche d'un élément dans un ABR équilibré, la complexité est la même que celle de la recherche dichotomique, c'est à dire $`\mathrm{O(ln(n))}`$. L'algorithme est donc très efficace.

Soit un ABR, de taille `n` et de hauteur `h`. 'n' étant le nombre total de noeuds, c'est aussi la quantité totale de données à traiter par l'algorithme de recherche.  
 Lors de la recherche un changement de profondeur s'effectue grâce à une opération élémentaire, comme une comparaison. Ainsi, le nombre d'opérations élémentaires pour trouver un élément est égal aux nombre de noeuds parcourus, donc à sa profondeur. Dans le pire des cas, c'est-à-dire si l'élément recherché se trouve tout en bas de l'arbre, le nombre d'opérations à effectuer est donc égal à la hauteur `h` de l'arbre.

Ainsi pour un ABR la complexité de l'algorithme de recherche est égal à sa hauteur `h`. Celle-ci dépend de `n` et de la géométrie de l'arbre.

Lorsque l'arbre est dégénéré, dit aussi filiforme ou peigne, sa hauteur `h` est égale à sa taille `n`. La complexité dans le pire des cas est donc $`\mathrm{O(n)}`$.

Lorsque l'arbre est équilibré, la performance de l'algorithme est bien meilleure. En effet à chaque changement de hauteur, la taille du nombre de noeuds restant à analyser est divisée par deux.

Le tableau ci-dessous donne la taille d'un ABR équilibré en fonction de sa hauteur.

|hauteur h  |taille n  |
|:-:|:-:|
|1  |n = 1|
|2  |n = 3|
|3  |n = 7|
|4  |n = 15|
|...|...    |
|h  |n =  2<sup>h</sup> - 1|

La dernière relation du tableau permet d'obtenir la hauteur en fonction de la taille :

$`\mathrm{n = 2^h - 1}`$

$`\mathrm{\Longleftrightarrow 2^h = n + 1}`$

$`\mathrm{\Longleftrightarrow log_{2}(2^h) = log_{2}(n + 1)}`$

$`\mathrm{\Longleftrightarrow h . log_{2}(2) = log_{2}(n + 1)}`$

$`\mathrm{\Longleftrightarrow h = \dfrac {1} {log_{2}(2)} . log_{2}(n + 1) \approx 1.44  . log_{2}(n + 1)}`$

Ainsi la complexité de l'algorithme de recherche dans un ABR est :

|ABR      |meilleur des cas|pire des cas      |
|:-------:|:--------------:|:----------------:|
|filiforme|O(1)            |O(n) = n          |
|équilibré|O(1)            |O(log_{2}(n))          |

# Ajout dans un ABR

Avant d'ajouter un élément, il faut apprendre à construire un ABR ... on va ajouter ainsi à partir d'un arbre vide.  
Ajouter un nouvel élément de valeur `x` dans un ABR dépend de sa valeur : on le place à gauche ou à droite.  
Afin d'éviter le cas particulier de l'arbre vide, on va renvoyer un nouvel arbre à chaque ajout.

La démarche est donc :  

* si l'arbre est vide, on renvoie un noeud unique, de valeur x
* on compare _x_ à la valeur du noeud _abr.valeur_ : si x est plus petit, on doit l'ajouter dans le sous-arbre gauche. __L'appel récursif__ ajoute(x,abr.gauche) nous renvoie un arbre contenant _x_ et tous les éléments de _abr.gauche_.
* si _x_ est plus grand que la valeur, on doit l'ajouter dans l'arbre droit.

```python
def ajoute(x,abr):
    '''ajoute x à l'arbre binaire (abr), renvoie un nouvel arbre
    '''
    if abr is None : 
        return Noeud(None,x,None)   # crée le premier noeud
    if x < abr.valeur :
        return Noeud(ajoute(x, abr.gauche) , abr.valeur , abr.droite)
    else : 
        return Noeud( abr.gauche , abr.valeur , ajoute(x, abr.droite))
```

# Encapsulation dans un objet  

On peut encapsuler les arbres binaires de recherche dans une classe nommée ABR à partir de la classe `Noeud` ( sur le même principe des listes chaînées où on utilisait `Cellule`).  

Chaque objet de la classe ABR contient un __unique attribut__ , `racine` qui est l'arbre binaire de recherche qu'il représente.  
On associera la __méthode `ajouter`__  : on remplace l'attribut `racine` par un nouvel arbre en utilisant la fonction __ajoute__ précédente ; ainsi que la __méthode `contient`__ en utilisant la fonction __appartient__ vue plus haut.
On peut ajouter aussi les autres méthodes comme __taille__ , __hauteur__ , __parcours_infixe__.  

```python

class ABR :
    def __init__(self ):
        self.racine = None
    
    def ajouter(self, x) :
        self.racine = ajoute(x, self.racine) 

    def contient(self, x) : 
        return appartient(x, self.racine)
```

Voici la visualisation ci-dessous de l'exécution suivante :  

 a = ABR() >>> a.ajouter(3) >>> a.ajouter(1) >>> a.ajouter(4) >>> a.ajouter(2)

![exemple](./img/class_ABR.jpg)  

Sources :  

* Cours NSI S Ramstein Timothée Decoster Lycée Quenot à Villeneuve Ascq
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen  
