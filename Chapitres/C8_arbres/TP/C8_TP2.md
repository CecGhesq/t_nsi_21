---
title: "Structure de données : 8_ Arbres binaires"
subtitle: "TP_2 : arbre binaire de recherche "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1  🏆

Donner tous les arbres binaires de recherche formés de trois noeuds et contenant les entiers 1, 2, 3.

# Exercice 2  🏆

1. Dans un ABR, où se trouve le plus petit élément? En déduire une fonction `minimum(a) ` qui renvoie le plus petit élément de l'ABR `a`. Si l'arbre est vide, alors cette fonction renvoie `None`.
2. Où se trouve le plus grand élément? En déduire une fonction `maximum(a) ` qui renvoie le plus grand élément de l'ABR `a`. Si l'arbre est vide, alors cette fonction renvoie `None`.

# Exercice 3  🏆

Ecrire une variante du programme `ajoute(abr, x)`du cours qui n'ajoute pas l'élément x si celui-ci est déjà dans l'arbre.  

# Exercice 4  🏆🏆

Soit l'arbre :  

![occurrence](./img/compte_occ.png)  

Ecrire une fonction `compte(x,a)` qui renvoie le nombre d'occurrences de x dans l'ABR précédent. Une valeur égale à la racine peut se trouver autant dans le sous-arbre gauche que dans le sous-arbre droit.  
On s'attachera tout de même à ne pas descendre dans les sous-arbres dans lesquels on est certain que la valeur x ne peut apparaître.  

```
>>> print('on trouve ',compte(4,a_occ), 'fois la valeur 4')
on trouve  2 fois la valeur 4
```


# Exercice 5 🏆🏆

1. Ecrire une fonction `remplir(a,t)` qui ajoute tous les éléments de l'arbre a dans le tableau `t` , dans l'ordre infixe. Chaque élément x est ajouté au tableau `t` avec `t.append(x)`.  

2. Ajouter ensuite une méthode lister(self) à la __classe ABR__ qui renvoie un nouveau tableau contenant tous les éléments de l'ABR self par ordre croissant.  

3. Ecrire une fonction `trier(t)` qui reçoit en argument un tableau d'entiers et renvoie un tableau trié contenant les mêmes éléments en utilisant les questions précédentes. Quelle est l'efficacité de ce tri?  

