---
title: "Structure de données : 1_Modularité"
subtitle: "TP "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : QCM &#x1F3C6;

1. Parmi les lignes qui suivent, trouver celle qui ne permet pas d'importer et d'utiliser la totalité du module `itertools`:  
[ ] import itertools  
[ ] import itertools as itt  
[ ] from itertools import cycle  

2. Pour importer deux fonctions __cos__ et __sin__  du module __math__, on peut écrire :  
[ ] from math import cos  sin  
[ ] from math import cos , sin  
[ ] from math import cos and sin  

3. Dans l'écriture __mystere.myst__ :  
[ ] `mystere` est une constante , `myst` est un module  
[ ] `mystere` est un module , `myst` est un module  
[ ] `mystere` est un module , `myst` est une constante  

# Exercice 2 : Création de modules &#x1F3C6; &#x1F3C6;

En Python, un module peut être un simple fichier. Ce fichier contient des définitions de fonctions, de constantes destinées à l'aide d'une documentation. On créera tout d'abord ce fichier avec l'extension .py

1. Créer ainsi un fichier __aires.py__ dans lequel on précise la valeur utilisée de `pi` ; et les définitions de fonctions pour calculer les aires d'un __disque(rayon)__, __rectangle(largeur, longueur)__, __triangle(base, hauteur)__  

```python
>>> disque(5)
78.53975
>>> rectangle(3,4)
12
>>> triangle(2,6)
6.0
```

2. Pour utiliser le module `aires` dans un autre programme, il faut l'importer : pour cela il faut préciser le chemin d'accès à l'interpréteur Python du __DOSSIER__ contenant aires.py

```python
from os import chdir  # importation os pour ouvrir le DOSSIER
#ouverture DOSSIER contenant aires ATTENTION / et non \ entre les répertoires
chdir ("C:/Users/mon_nom/C1/TP/")
```

On peut aussi enregistrer le fichier aires.py dans le même dossier pour une importation plus facile.
Après importation, afficher dans la console la documentation de la fonction disque ainsi que la valeur de disque(5)

3. Afficher le contenu du module à l'aide de la fonction `dir()`  

4. Reprendre le fichier aires.py et ajouter les lignes suivantes :

```python
print("calcul d'aires")
print("Disque : pi*r^2")
print("par exemple, avec r = 5")
print ("Aire = pi*5^2 =", disque(5))
```

Relancer un fichier avec importation du module `aires`,  et observez ce qu'il se passe en console lorsque vous demandez la valeur de disque(5).

5. Pour éviter ce problème, on place la partie qui ne doit pas être affichée dans une fonction nommée `main`et on ajoute une condition :  

```python
def main():
    print("calcul d'aires")...etc..

if __name__== "__main__":
    main()
```

Réaliser à nouveau la question précédente et observez la différence.  

>__📢 A retenir :__  
Grâce à l'instruction `if __name__== "__main__":    main() :  

>    Une variable `__name__` est crée automatiquement à l'exécution  d'un programme courant quand on importe un programme. Mais le nom du programme principal est toujours `__main__` qui n'est exécuté que si ce fichier est le fichier principal. Si ce fichier est par contre importé dans un autre programme, la condition n'étant pas vérifiée, cette fonction main() n'est pas exécutée.

# Exercice 3 : modules standards  
Le module copy permet la copie d'une liste.  

1. Importez dans une console ce module. A partir de la description de ce module `(help(copy))` , indiquez l'interface sommaire de celui-ci.  

2. Comment peut-on utiliser ce module pour réaliser la copie superficielle de la liste l1=["Emma", "Tom", "Maxence"] ? Testez vos lignes de code dans la console de « Thonny ».

3. Recommencer avec cette liste 

```python
l1_ = [["Emma",2007],["Tom", 2009] ,["Maxence", 2005] ]
```

4. Quelle différence importante existe-t-il entre la copie superficielle et la copie récursive ? Vérifiez sur le site de « Python Tutor » le résultat de la copie superficielle et de la000000000000 copie récursive de la liste l1_.

[lien Python Tutor](https://urlz.fr/gp4Q)

5. Dans quel dossier dans le disque dur de votre système d’exploitation, le module « copy.py » a-t-il été enregistré ? Regardez dans les informations pour le connaitre.

# Exercice 4 : Utilisation d'une bibliothèque &#x1F3C6; &#x1F3C6;

Dans le domaine du traitement de l'image, nous allons nous intéresser à la bibiothèque __PIL__. Pour comprendre le fonctionnement il faut lire la documentation :
 [ici](https://pillow.readthedocs.io/en/stable/handbook/tutorial.html) ou [pdf](./documents/pillow-readthedocs-io-en-latest.pdf).  
Nous disposons d'une image sous la forme d'un fichier [Lena.jpg](./documents/Lena.jpg) que nous voudrions transformer en niveaux de gris, avec la même taille et l'enregistrer sous un fichier png.
1. Chercher dans la documentation les informations nécessaires pour ces opérations :  

* Quel module doit-on utiliser pour manipuler des images ?  

* Le format JPEG est-il accepté en lecture ?  

* Quelle fonction doit être utilisée ?  

* Quelle fonction d'enregistrement devra être utilisée?


2. Pour convertir en niveaux de gris, une fonction transforme les trois canaux de chacun des pixels rouge(R), vert(V), bleu(B) selon la formule :  

__L = R * 299/1000 + G * 587/1000 + B * 114/1000__

Cette formule est celle la plus utilisée dans les filtres selon la méthode CCIR 601.
Trouver le nom de la fonction nécessaire pour cette convertion en mode L  

3. Implémenter le code en python en suivant les étapes suivantes :  

* importer la bibiothèque `Image`
* charger l'image  Lena.jpg dans une variable
* convertir l'image en niveaux de gris
* enregistrer l'image modifiée en extension .png
* libérer la mémoire (on utilisera `variable_image_1.close()`et `variable_image_2.close()`)

---

Sources :  

* NSI Terminale Prépabac Hatier  
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen  
* NSI Terminale Ellipses S Bays  
* NSI Terminale Ellipses JC Bonnefoy B Petit  
