---
title: "Structure de données : Chapitre 1 Modularité"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
![BO](./img/BO_C1.jpg) ![BO](./img/BO_C1_2.jpg)  

Une des clés du développement à grande échelle de programme consiste à circonscrire et séparer proprement les parties d'un programme.  Il simplifie les tests, permet de réutiliser du code et facilite la maintenance.

![mod1](./img/mod1.jpg)  
Ainsi un programme  :

- code un ou plusieurs algorithmes  
- réalise une ou plusieurs fonctions

Son __interface__ est l'énumération de ces fonctions.  
Sa __réalisation__ est le code lui-même  

# Les modules en Python

Certains modules Python sont installés par défaut (bibliothèque standard) et d'autres peuvent être ajoutés en utilisant l'outil `pip` en ligne de commande ou dans Thonny dans Outils--> Gérer les paquets.  
Dans les bibliothèques les plus communes, on peut citer :

* Tkinter : affichage graphique
* Math : calcul mathématique
* PIL : traitement d'images
* Random : traitement de données aléatoires
* Time : gestion du temps
* package matplotlib avec divers modules pylab ou pyplot : programmation scientifique, graphiques

Un module peut être un fichier unique (extension py).

__📢 A retenir :__
> Pour importer un module installé, on utilise le mot clé `import`.  

>* __Méthode 1__ : 
Les fonctions doivent être __préfixées__ du nom du module.  
![méthode1](./img/methode1.jpg) 

\newpage

>* __Méthode 2__ :  
On __renomme__ le module avec un mot plus court qu'on utilise comme préfixe.  
![méthode2](./img/methode2.jpg) ![methode2bis](./img/methode2_b.jpg)  

>* __Méthode 3__ :    
on __importe uniquement__ la fonction désirée.  
![méthode3](./img/methode3.jpg)  

_Remarque_ : toute syntaxe avec `*` est à banir dans la plupart des cas : cela alourdit inutilement le fichier et la maintenance du programme peut être difficile. ( pas de " from random import *"!!!).  
On utilisera cette commande pour Tkinter ou micropython car ces modules sont des cadres de travail (encore appelés framework) .

La documentation du module s'obtient avec la fonction `help()`
```python
>>> import math
>>> help(math)
Help on built-in module math:

NAME
    math

DESCRIPTION
    This module provides access to the mathematical functions
    defined by the C standard.

FUNCTIONS
    acos(x, /)
        Return the arc cosine (measured in radians) of x.
    
    acosh(x, /)
        Return the inverse hyperbolic cosine of x.
        ...
```

Le contenu d'un module s'obtient avec la fonction `dir` :  

```python
>>> dir(math)
['__doc__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'comb', 'copysign', 'cos', 'cosh', 'degrees', 'dist', 'e', 'erf', 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd', 'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'isqrt', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'perm', 'pi', 'pow', 'prod', 'radians', 'remainder', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau', 'trunc']
```

# Interfaces

## Définition

Pour chaque module, on distingue __réalisation__ ( on dit aussi souvent __implémentation__), c'est à dire le code lui-même, et son __interface__, consistant en une énumération des fonctions définies dans le module qui sont destinées à être utilisées dans la réalisation d'autres modules, appelés __clients__  

>__📢 A retenir :__  
>L'interface d'un module est liée à sa documentation, et doit expliciter ce qu'un utilisateur a besoin de connaître des fonctions proposées : comment et pour quoi les utiliser.

## API

Une API, en anglais, __A__ pplication __P__ rogramming __I__ nterface, est une __interface__ de programmation d'application. Elle est destinée à être __utilisée par des programmes__. Le principe de ce type d'interface est le même que celui des UI ( User Interface) ou des GUI ( Graphical User Interface) destinées elles à un utilisateur humain.
Elle sert de lien entre un programme et les programmes qui vont l'utiliser. Elle peut être proposée par un service Web avec une documentation décrivant l'utilisation qui permettra la communication et l'échange de données.  

Pour chaque fonction de l'interface, on a besoin de :

* son nom
* sa liste de paramètres
* de sa spécification  

Des informations supplémentaires peuvent être apportées comme le temps d'exécution ou l'espace mémoire.

# Encapsulation
Les fonctions sont encapsulées dans le module.  
L'__auteur__ d'un module est libre de réaliser l'interface et de la mettre à jour tant que les résultats soient ceux décrits. L'auteur peut ainsi utiliser tout un lot de fonctions ou objets qui ne seront pas utilisés par le client : on qualifie l'interface de __privée__. Les autres qualificatifs sont __public__ et __protégé__.

Rq : en Python les éléments privés commencent par convention par _ _ ; ils ne sont pour autant pas davantage protégés et un client peut modifier un module par erreur.... A VOIR en TP

Le __client__ du module utilise l'interface sans forcément connaître sa réalisation, ni les mises à jour.

# Spécification documentation

__📢 A retenir :__
>La spécification d'une fonction contient toute la documentation nécessaire pour l'utiliser correctement. S'il ne faut pas devoir lire le corps de la fonction pour comprendre ce qu'elle fait et comment l'utiliser, c'est que la spécification est bien écrite.  


__Sources__ :  

* Cours NSI S Ramstein Lycée Quenot à Villeneuve d'Ascq  
* Cours NSI A Wilm Lycée Beaupré à Haubourdin  
* NSI Terminale Prépabac Hatier  
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen  
* NSI Terminale Ellipses S Bays  
* NSI Terminale Ellipses JC Bonnefoy B Petit  
