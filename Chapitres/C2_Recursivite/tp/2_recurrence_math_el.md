---
title: "Langage et programmations : 2_Récursivité"
subtitle: "Récurence (math)"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
# Exercice 1 : récurrences élémentaires &#x1F3C6;

1. Montrer que les propriétés suivantes sont héréditaires  

* Pour tout entier n≥1, $`3^{4n} - 1`$ est un multiple de 5.
* Pour tout entier n≥1, $`3^{4n} + 1`$ est un multiple de 5.
* Pour tout entier n≥1, $`10^{n} - 1`$ est un multiple de 9.  
* Pour tout entier n≥1, $`10^{n} + 1`$ est un multiple de 9.

2. Quelles sont, parmi les propriétés précédentes, celles qui sont vraies pour tout entier n >0 ?

# Exercice 2 : Sommes de termes  

Montrer que pour tout entier n :

* $`1 + 2 + 3 + .....+n = \frac{n(n+1)}{2}`$
* $`1^{2}+2^{2}+.........+ n^{2}= \frac{n(n+1)(2n+1)}{6}`$  
* $`1\times 2 + 2\times 3 + ⋯ + n(n+1)=\frac{n(n+1)(n+2)}{3}`$  

# Exercice 3 : récurrence forte :
On considère la suite (un) n ∈ N définie par :

$`\begin{Bmatrix} u_{0} = 1 \\u_{n+1}= u_{0} + u_{1} + ... +u_{n} \end{Bmatrix} Montrer\: que\: pour\: tout \: entier \:  n \:,u_{n} \leq 2^{n}`$
