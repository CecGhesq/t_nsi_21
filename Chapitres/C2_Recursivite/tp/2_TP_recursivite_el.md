---
title: "Langage et programmations : 2_Récursivité"
subtitle: "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
# Exercice 1 : QCM &#x1F3C6;
1.- On considère la fonction suivante :  

```python
def f(n) :
    if n == 0:
        return 1
    else :
        return 2 * f(n-1)
```
a- Comment est évalué f(8)?  
[ ] ce calcul provoque une erreur    
[ ] cette expression vaut 14   
[ ] cette expression vaut 256    
[ ] cette expression vaut 2    

b- Cette fonction correspond à :  
[ ] n!  
[ ] $`2^{n}`$  
[ ] 2*n  
[ ] $`n^{2}`$

2.- On considère la fonction suivante :  

```python
def inverse(lst) :
    return inverse(lst[1:]) + [lst[0]]
```  

Cette fonction  :  
[ ] renvoie l'image miroir d'une liste.   
[ ] a une complexité en O(n)   
[ ] a une complexité en $`O(n^{2})`$    
[ ] renvoie un erreur    

3.- Une complexité décrite par une relation de récurrence de la forme c(n) = C(n-1) + n et C(1) = 1 est en :  
[ ] O(n)  
[ ] $`O(n^{2})`$  
[ ] $`O(2^{n})`$   
[ ] O(nlogn)  

# Exercice 2 : Factorielle &#x1F3C6;

En mathématiques, la fonction factorielle est une fonction définie sur $`\mathbb{N}^{\ast}`$ qui à n associe :  
$`n! = 1 \times 2\times 3\times 4\times...\times(n-1)\times n.`$  
On montre ainsi que la relation de récurrence $`n! = n \times (n-1)!`$ : si on sait calculer (n-1)! on connaîtra la valeur de n!  
De même $`(n-1)! = (n-1) \times(n-2)!`$   on est donc ramené au calcul de (n-2)! etc.... jusque 1.  
Remarque : on considère que 0! = 1

1.- Ecrire le programme récursif en python.
  
2.- Compléter l'execution de cette fonction à l'aide de la notation __pile__ sur l'exemple de `factorielle(4)`:    
![piles](./img/piles_ex2_à_comp.jpg)

3.- Rendre terminal l'algorithme récursif.  

# Exercice 3 : La conjecture de Syracuse  [^1] &#x1F3C6; &#x1F3C6;
En mathématiques, on appelle suite de Syracuse une suite d'entiers naturels définie de la manière suivante :  

* on part d'un nombre entier strictement positif ;  
* s’il est pair, on le divise par 2 ;  
* s’il est impair, on le multiplie par 3 et on ajoute 1.  

En répétant l’opération, on obtient une suite d'entiers strictement positifs dont chacun ne dépend que de son prédécesseur.  
Ecrire une fonction récursive `syracuse(u_n)`qui affiche les valeurs successives de la suite $`u_{n}`$  est plus grand que 1. On placera des `assert` afin de controler la valeur d'entrée.  
_Remarque : la conjecture de Syracuse affirme que, quelle que soit la valeur de $`u_{0}`$, il existe un indice n dans la suite tel que $`u_{n}= 1`$. Cette conjoncture défie toujours les mathématiciens._  

# Exercice 4 &#x1F3C6;  

Ecrire une fonction __récursive__ `boucle(i,k)` qui affiche les entiers entre _i_ et _k_.  

```python
>>> boucle(3,5)
 3
 4
 5
```  

# Exercice 5 &#x1F3C6;  &#x1F3C6;

1. Ecrire une fonction __récursive__ `nombre_de_chiffres(n)` qui prend un entier positif ou nul `n` en argument et renvoie son nombre de chiffres.  
Remarque : $`un \: nombre \: possédant\: un\: chiffre\: est\: \leq 9`$

```python
>>> print(nombre_de_chiffres(1234569))
7
```

2. Ecrire une fonction __récursive__ `nombre_de_bits1(n)` qui prend un entier positif ou nul et renvoie le nombre de bits valant __1__ dans la représentation binaire de `n`. 

# Exercice 6  Le triangle de Pascal &#x1F3C6;  &#x1F3C6;

| | |
|:---:|:---:|
|En mathématiques, le triangle de Pascal est une présentation des coefficients binomiaux dans un triangle. Il fut nommé ainsi en l'honneur du mathématicien français Blaise Pascal. Il est connu sous l'appellation « triangle de Pascal » en Occident, bien qu'il ait été étudié par d'autres mathématiciens, parfois plusieurs siècles avant lui, en Inde, en Perse (où il est appelé « triangle de Khayyam »), au Maghreb, en Chine (où il est appelé « triangle de Yang Hui »), en Allemagne et en Italie (où il est appelé « triangle de Tartaglia »).  https://fr.wikipedia.org/wiki/Triangle_de_Pascal |![triangle](./img/triangle_pascal.jpg)|  


Le triangle est défini de manière récursive, pour tout entier 0 $`\leq \:  p \: < n`$  , ainsi :  

$`C(n,p) =\left\{\begin{matrix} 1 & si\, p = 0\: \, ou \: \, n\, = p\, , \\ C(n-1,p-1) + C(n-1,p)) & sinon \end{matrix}\right \:`$

Ecrire une fonction __récursive__ C(n,p) qui renvoie la valeur de C(n,p) puis dessiner le triangle de Pascal à l'aide d'une double boucle `for` en faisant varier _n_ entre _0_ et _10_.

# Exercice 7 La flocon de Koch &#x1F3C6;  &#x1F3C6; &#x1F3C6;

On peut créer la courbe de Koch à partir d'un segment de droite, en modifiant récursivement chaque segment de droite de la façon suivante :  

* On divise le segment de droite en trois segments de longueurs égales.  
* On construit un triangle équilatéral ayant pour base le segment médian de la première étape.  
* On supprime le segment de droite qui était la base du triangle de la deuxième étape.  

| | |
|:---:|:---:|
|Au bout de ces trois étapes, l'objet résultant a une forme similaire à une section transversale d'un chapeau de sorcière. La courbe de Koch est la limite des courbes obtenues, lorsqu'on répète indéfiniment les étapes mentionnées ci-avant. | ![triangle](./img/Droite_Koch.png)| 


![triangle](./img/Koch_anime.gif)

Ainsi le cas de base à l'ordre n = 0 de la récurrence est simplement le dessin d'un segment de longueur l.
Le cas récursif d'ordre n s'obtient en divisant ce segment en 3 morceaux de même longueur l/3, puis en dessinant la forme triangle sans la base. 
On réitère ce processus à l'ordre n-1 pour chaque segment ( de longueur l/3 ).  

1. Ecrire une fonction `koch(n,l)` qui dessine avec _Turtle_ un flocon de Koch de profondeur `n` à partir d'un segment de longueur `l`.  
[Voir ici le site académique sur le module Turtle](http://fe.fil.univ-lille1.fr/apl/2018/turtle.html)  
2. Le flocon de Koch se code à partir de 3 courbes de Koch réparties selonun triangle équilatéral. Coder `flocon_koch(n,l)`à partir de `koch(n,l)` . 

Sources :  

* Cours NSI S Ramstein Lycée Quenot à Villeneuve d'Ascq  
* Cours NSI A Wilm Lycée Beaupré à Haubourdin    
* NSI Terminale Prépabac Hatier    
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen    
* NSI Terminale Ellipses S Bays  
* NSI Terminale Ellipses JC Bonnefoy B Petit  

[^1]: https://fr.wikipedia.org/wiki/Conjecture_de_Syracuse
