---
title: "Struture de données : 5_Pile_File"
subtitle: "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


📌 METHODE  
Réaliser un module __pile.py__ puis un module __file.py__  dans votre dossier afin d'appeler les constructeurs dans les exercices.

# Exercice 1 : QCM &#x1F3C6;

1. Quelle opération ne fait pas partie de l'interface d'une pile ?

[ ] ajouter un élément à une pile  
[ ] retirer l' élément le plus récent à une pile  
[ ] retirer l'élément le plus ancien à une pile

2.  Quelle opération ne fait pas partie de l'interface d'une file ?

[ ] ajouter un élément à une file  
[ ] retirer l' élément le plus récent à une file  
[ ] retirer l'élément le plus ancien à une file   

3. Sur les quatre situations suivantes, choisir parmi les réponses suivantes la structure la plus adaptée:  

[ ] une liste  
[ ] un dictionnaire  
[ ] une pile  
[ ] une file  

a. on souhaite insérer dans une structure de données les différentes hauteurs d'un arbre au fil des années sans y insérer les années. 
b. dans une administration, on souhaite insérer dans uns structure de données les doléances afin de les traiter selon leur ordre d'arrivée.  
c. dans une association, on souhaite enregistrer une liste d'informations pour chaque adhérent : taille, poids, etc.  
d. pour un logiciel, on doit enregistrer la liste des actions dans une structure de données de sorte à voir la dernière action en priorité.  

# Exercice 2 : Navigateur Web &#x1F3C6; A REVOIR
Considérons un navigateur Web dans lequel on s'intéresse à deux opérations : aller à une nouvelle page et revenir à la page précédente. On veut que le bouton de retour en arrière permette de remonter une à une les pages précédentes, et ce jusqu'au début de la navigation.  
Soient les trois URL visitées précédemment : 

* __adresse_1__ https://search.lilo.org/results.php?q=JO+fleuret 
* __adresse_2__ https://www.leparisien.fr/sports/JO/jo-de-tokyo-les-fleurettistes-francaises-en-argent-battues-en-finale-par-la-russie-29-07-2021-7Q7ETJSHRRGBLCSFGXDFCAX5TM.php
* __adresse_3__ https://www.leparisien.fr/sports/JO/direct-jo-de-tokyo-les-skaters-francais-en-revent-un-dimanche-de-medailles-25-07-2021-5CKJO2SWMFGJ5NYDB3KOJEAGRQ.php  



1. Quelle structure de données allez vous utiliser? Importer le module éventuellement dans Thonny avec l'implémentation en liste chaînée.

2. En plus de l'`adresse_courante` ( la troisième URL ici) qui doit être stockée, créer la structure de données avec les `adresses_précédentes`( les deux premières URL).

3. Quand on navigue, l'adresse de la page courante doit être ajoutée à la structure de données et ensuite l'adresse cible devient la nouvelle adresse courante : créer la fonction `aller_a(adresse_cible)`. La tester avec l'url cible en affichant la nouvelle `adresse_courante` : __adresse_4__ "https://www.leparisien.fr/sports/JO/jo-de-tokyo-charline-picon-visera-lor-samedi-en-planche-a-voile-29-07-2021-7JKAUXLSTNCXDNDK7SLSVWABZE.php"

4. Pour revenir en arrière, il faut revenir sur la dernière page. On vérifie tout d'abord qu'elle existe dans la structure de données avant de la renvoyer. Tester en affichant deux retours en arrière.

5. On souhaite avoir une fonction `retour_avant` dont le comportement est :  

* chaque appel à la fonction `retour` place la page quittée au sommet d'une seconde pile `adresses_suivantes`
* un appel à la fonction `retour_avant` amène la page enregistrée au sommet de la pile `adresses_suivantes` et met à jour les deux piles de façon adaptée,
* toute nouvelle navigation avec `aller_a` annule les `adresses_suivantes`. 

Afin de tester , aller à l'adresse_3  puis réaliser un retour puis un retour avant. Vous devez revenir à la même adresse.  


# Exercice 3 : Bon parenthésage ? &#x1F3C6;  

Une expression ( algébrique ou arithmétique) est correctement parenthésée si d'une part, le nombre de parenthèses (ou crochets) ouvrant.e.s et fermant.e.s est le même et si d'autre part les correspondanteces ne se croisent pas.

```python
>>> is_good_par("( 2*[3 + 2] )")
True
>>> is_good_par("( 2*[3 + 2) ]")
False
```

1. Ecrire un algorithme qui s'aide d'une pile pour contrôler le bon parathésage d'une expression.

2. Ecrire en Python une fonction `is_good_par( E )` qui renvoie "True" si l'expression E est correctement parenthésée, et False dans le cas contraire.   

# Exercice 4 : Compléments de la classe pile &#x1F3C6; 

1. Réaliser une méthode `consulter` ( permet de connaître la valeur du sommet de la pile), `vider` (définir le contenu à vide),et  placer celles-ci dans votre module pile.  
2. Réaliser une méthode permettant d'inverser l'ordre dans une pile.

## Exercice 5 : Notation polonaise inversée &#x1F3C6; &#x1F3C6;   
La notation polonaise inversée (NPI) permet d'écrire des opérations arithmétiques, sans utiliser de parenthèses. Ici, nous nous limiterons à des nombres entiers naturels et aux opérations + , - , * , / sur eux. Dans cette notation, les opérateurs sont écrits après les opérandes ( nombres entiers naturels).  
Ainsi 13*(3 + 2) devient en NPI : 3 2 + 13 *  

On écrit et on exécute les opérations dans le sens des priorités habituelles. Ainsi :  

* l'addition entre 3 et 2 (3 2 +)
* la multiplication entre le précédent résultat et 13 ( 13 * )
* on a ainsi le résultat
 
 1. Donner la File correspondante à la saisie NPI correspondante à l'exemple. Faire de même avec la Pile.
 
 2. Quelle est la structure adaptée à l'exercice ?
 

 3. Proposer une fonction permettant d'afficher le résultat d'une expression en NPI ( expression en chaîne de caractères) composée d'additions et de multiplications de nombres entiers.

Rq : expression.split(" ") permet de convertir une chaine de caractères en liste avec chaque caractère si ceux-ci sont séparés par un espace (" "). On pourra ainsi parcourir la liste des caractères plutôt que d'inverser une pile.

```python
>>> x = "2 3 4 * 2 +".split(" ")
>>> x
['2', '3', '4', '*', '2', '+']
```

Aide : on  utilisera une pile pour stocker les résultats intermédiaires. On observe  1 à 1 les éléments de l'expression et on effectue les actions suivantes :  

* si on voit un nombre, on le place sur la pile  
* si on voit un opérateur binaire, on récupère les deux nombres au sommet de la pile, on leur applique l'opérateur, et on replace la résultat sur la pile.  

```python
>>> NPI("1 2 3 * + 4 *")
28.0
```

La notation classique serait (1 + 2 * 3) * 4


# Exercice 6 : Pile bornée
Une pile bornée est une pile dotée d'une capacité maximale à sa création. Soit l'interface suivante :  

|fonction| description|
|:---:|:---|
|creer_pile(c)| crée et renvoie une pile bornée de capacité c|
|est_vide(p)| renvoie _True_ si la pile est vide et _False_ sinon |
|est_pleine(p)|renvoie _True_ si la pile est pleine et _False_ sinon |
|empiler(p,e)|ajoute _e_ au sommet de _p_ si _p_ n'est pas pleine, et lève une exception _IndexError_ sinon |
|depiler(p)|retire _e_ au sommet de _p_ si _p_ n'est pas vide, et lève une exception _IndexError_ sinon |

On propose de réaliser une telle pile avec une __liste__ (type list en Python) dont la taille est fixée à la création et correspond à la capacité.  
Les éléments dans la pile sont stockés à partir de l'indice 0 ( qui contient le fond de pile ). On se donne également un entier enregistrant le nombre d'éléments dans la pile, qui permet donc de désigner le prochain indice libre.  

|||||||||
|---|---|---|---|---|---|---|---|
|a||b|...|z| None|None|...|None|


# Exercice 7 : File bornée
Une file bornée est une file dotée d'une capacité maximale à sa création. Soit l'interface suivante :  

|fonction| description|
|:---:|:---|
|creer_file(c)|crée et renvoie une file bornée de capacité c|
|est_vide(f)| renvoie _True_ si la file est vide et _False_ sinon |
|est_pleine(f)|renvoie _True_ si la file est pleine et _False_ sinon |
|ajouter(f,e)|ajoute _e_ à l' arrière de _f_ n'est pas pleine, et lève une exception _IndexError_ sinon |
|retirer(f)|retire et renvoie  _e_ à l'avant  de _f_ si _f_ n'est pas vide, et lève une exception _IndexError_ sinon |

Comme pour la pile bornée, on propose de réaliser une telle file avec une __liste__ (type list en Python) dont la taille est fixée à la création et correspond à la capacité.  
Les éléments dans la file sont stockés à partir de l'indice __premier__ ( qui correspond à l'avant de la file). Le tableau est considéré comme __circulaire__ : après la dernière case, les éléments reviennent à la première.  
_Aide_ : on utilisera la formulation classique utilisant le __reste de la division par la longueur du contenu__.  

On se donne également un entier enregistrant le nombre d'éléments dans la file, qui permet donc de désigner le prochain indice libre.  
Par exemple, si on choisit __premier__ à 2 et qu'on ajoute les lettres de a à e : 

```python
>>> f_b = File_bornee(6)
>>> f_b.ajouter("a")
>>> print(f_b.contenu)
[None, None, 'a', None, None, None]
>>> f_b.ajouter("b")
>>> print(f_b.contenu)
[None, None, 'a', 'b', None, None]
>>> f_b.ajouter("c")
>>> print(f_b.contenu)
[None, None, 'a', 'b', 'c', None]
>>> f_b.ajouter("d")
>>> f_b.ajouter("e")
>>> print(f_b.contenu)
['e', None, 'a', 'b', 'c', 'd']
>>> print(f_b.nombre)
5
```

_Rq : L'indice de e est bien "0" car correspond au reste de la division à partir de la somme de premier au nombre d'éléments divisé par la longueur de la file._  

Si on retire, l'élément d'indice `premier` est enlevé de la la file ( None placé ) , `premier` évolue, ainsi que `nombre`. 
```python
>>> f_b.retirer()
'a'
>>>print(f_b.premier)
3
>>> print(f_b.contenu)
['e', None, None, 'b', 'c', 'd']
>>> print(f_b.nombre)
4
```

Réaliser cette structure à l'aide d'une classe ayant pour attribut un tableau fixe, le nombre d'éléments dans la file bornée et l'indice du premier élément.


Sources :  
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen
* NSI Terminale Nathan les vrais exos S Pasquet M Leopoldoff

