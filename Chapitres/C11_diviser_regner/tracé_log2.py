import matplotlib.pyplot as plt
from math import log
x = [i for i in range(1,1000)]
y = [log(i, 2) for i in x]

plt.plot(x, y) #construction de la courbe (sans affichage)
plt.xlabel("x") # Nom de l'axe des abscisses
plt.ylabel("y") # Nom de l'axe des ordonnées
plt.title(" f(x) = log_2(x)")  # titre du graphique
plt.show() # affiche la figure à l'écran
