---
title: " : Algorithmique : 11 Diviser pour régner"
subtitle: "TP "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
# Exercice 1 🏆

1. Détaillez les étapes du tri fusion sur le tableau [23, 17, 28, 11, 20, 22, 19, 16].  

```

















```

2. Déterminez le nombre de comparaisons effectuées durant les étapes de fusion.

```







```


3. Combien faudrait-il faire de comparaisons avec l'algorithme de tri par sélection ?
```



```

# Exercice 2 🏆

1. Détaillez les étapes du tri fusion sur le tableau [68, 46, 27, 54, 32].  

```
















```

2. Déterminez le nombre de comparaisons effectuées durant les étapes de fusion.  

```







```

3. Combien faudrait-il faire de comparaisons avec l'algorithme de tri par sélection ?  

```



```

# Exercice 3 🏆

On considère la fonction rechercher(x,T,debut,fin) de recherche, entre debut
et fin inclus, de l'indice d'un élément x dans un tableau T trié suivant l'ordre croissant.

```python
def rechercher(x, T, debut, fin):
""" Renvoie l'indice de x, entre debut et fin, dans le
tableau trié dans l'ordre croissant T, et -1 sinon. """
    while debut <= fin:
        milieu = (debut + fin) // 2
        if x == T[milieu]:
            return milieu
        elif x < T[milieu]:
            fin = milieu - 1
        else:
            debut = milieu + 1
    return -1

def indice(x, L):
    return rechercher(x, L, 0, len(L)-1)

premiers = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59]
>>> print(indice(19, premiers))
7
>>> print(indice(51, premiers))
-1 

```

1. De quel algorithme s'agit-il ?  

```


```

2. En quoi cet algorithme relève-t-il du paradigme diviser-pour-régner ?  

```








```

3. Réécrire la fonction rechercher(x,T,debut,fin) sous la forme récursive.  


# Exercice 4 🏆🏆
Les règles de calcul sur les puissances entières positives d'un nombre a
permettent d'établir les formules suivantes.

$`a^{n} = \left\{\begin{matrix} a& si\;  n = 1  \\  (a^{2})^{n/2}& si \, n\, est\, pair \\  a\times (a^{2})^{(n-1)/2}& si \, n\, est\, impair \\ \end{matrix}\right.`$  

En appliquant ces formules suivant la parité de l'exposant n, on peut calculer la puissance d'un nombre a en effectuant des multiplications simples et des mises au carré.  
Par exemple $`a^{14}`$ s'obtient en calculant $`(a^{2})^{7}`$ car 14 est pair et 14÷2 = 7.
Autrement dit a à la puissance 14 est égal à a² à la puissance 7.  

De même $`a^{7}`$ s'obtient en calculant $`a\times (a^{2})^{3}`$ car 7 est impair et 7÷2 = 3.  
Ainsi a à la puissance 7 est égal au produit de a par a² à la puissance 3.

Le but de cet exercice est d'implémenter cet algorithme de calcul, appelé __exponentiation rapide__, sous la formule d'une fonction récursive puissance(a,n) qui doit renvoyer le résultat de _a élevé à la puissance n_ (un entier naturel non nul).  

1. Exprimer les formules données au début en appels à la fonction
puissance() en précisant à chaque fois la condition s'appliquant à n en Python.  
```






```

2. Rédiger la fonction puissance(a,n)  

3. En quoi cet algorithme relève-t-il du paradigme diviser-pour-régner ?  

```







```

# Exercice 5 🏆🏆

Les règles de calcul sur la multiplication des entiers positifs a et b permettent d'établir les formules suivantes.  
$`a\times b \left\{\begin{matrix} (a \times 2 ) \times (b\div 2)& si\: b\: est\: pair \\ a \: +\: (a \times 2 ) \times ((b-1)\div 2)& si\: b\, est\: \: impair \\\end{matrix}\right.`$

En appliquant ces deux formules suivant la parité du nombre b, on peut calculer le produit d'un nombre a avec un nombre b en effectuant des additions et des multiplications / divisions par 2 (c'est à dire des décalages de bits).
Vous devez implémenter cet algorithme de calcul, appelé multiplication russe, sous la formule d'une fonction récursive produit(a,b) qui doit renvoyer le produit des deux entiers naturels non nuls a et b.

__Remarque__ : C'est une version « additive » de l'exponentiation rapide...  

# Exercice 6 🏆🏆

Le tri rapide (quicksort en anglais) est un algorithme de tri généralement très rapide qui met en oeuvre le paradigme diviser-pour-régner suivant l'algorithme :

* Lorsqu'un tableau est vide ou ne contient qu'un élément, il est déjà trié.
* Sinon :
    * Prendre l'élément (appelé pivot) situé au milieu du tableau T à trier.
    * Créer le tableau Tinf des éléments de T strictement inférieurs au pivot.
    * Créer le tableau Tpivot des éléments de T égaux au pivot.
    * Créer le tableau Tsup des éléments strictement supérieurs au pivot.
* Le tableau trié est obtenu par la concaténation des trois tableaux :
Tinf trié, Tpivot non trié et Tsup trié suivant le même algorithme.

Implémentez le tri rapide sous la forme d'une fonction récursive tri_rapide() en utilisant des compréhensions pour créer le trois sous-tableaux. 


# Exercice 7 🏆🏆🏆

1. Combien de comparaisons faut-il effectuer en tout pour trouver à la fois le plus petit et le plus grand élément d'un tableau de 15 éléments ?

```



```

2. On souhaite créer une fonction récursive min_et_max(T) qui renvoie le minimum et le maximum du tableau non vide T sous la forme d'un tuple (minimum,maximum).
Quel tuple, la fonction min_et_max(T) doit-elle renvoyer, lorsque le tableau T ne contient qu'un seul élément ?  

```


```

3. Implémentez la fonction min_et_max(T), en suivant le paradigme diviser pour régner. Vous vous servirez du slicing Python pour scinder le tableau T en deux, et vous comparez les résultats renvoyés par les appels récursifs sur le deux soustableau pour établir le minimum et le maximum à renvoyer.

Source :
* Cours NSI Jean Diraison  
